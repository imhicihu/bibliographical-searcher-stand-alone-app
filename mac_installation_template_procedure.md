By default, Mac OS only allows users to install applications from `verified sources`. In effect, this means that users are unable to install most applications downloaded from the internet or stored on physical media without receiving the error message below:

![trash.png](https://bitbucket.org/repo/yprLRxE/images/4253502906-Capture%20d%E2%80%99e%CC%81cran,%20le%202020-09-03%20a%CC%80%2015_47_2.jpg)

Open the System Preferences. This can be done by either clicking on the System Preferences icon in the Dock or by going to `Apple Menu` > `System Preferences`.
Open the `Security & Privacy` pane by clicking `Security & Privacy`.
Make sure that the General tab is selected. Click the icon labeled `Click the lock to make changes`.

![trash.png](https://bitbucket.org/repo/yprLRxE/images/2050459796-Capture%20d%E2%80%99e%CC%81cran,%20le%202020-09-03%20a%CC%80%2015_47_3.jpg)

Enter your username and password into the prompt that appears and click `Unlock`.

![trash.png](https://bitbucket.org/repo/yprLRxE/images/3357284949-44.jpg)

Under the section labeled `Allow applications downloaded from`:, select `Anywhere`. On the prompt that appears, click `Allow from Anywhere`.

![trash.png](https://bitbucket.org/repo/yprLRxE/images/4049989995-Capture%20d%E2%80%99e%CC%81cran,%20le%202020-09-03%20a%CC%80%2015_47_5.jpg)

Exit System Preferences by clicking the red button in the upper left of the window. You should now be able to install applications downloaded from the internet.

------------------------
Note for OSX users: if OSX tells you `Biblio.app` cannot be opened because the developer cannot be verified., you don't need to redownload it. Instead, you need to allow OSX to run the unsigned app. To do this, right click the application in Finder and select Open. Then click Open in the popup window that appears. You only need to do this once.