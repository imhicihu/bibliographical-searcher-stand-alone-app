![stability-workinprogress](https://bitbucket.org/repo/ekyaeEE/images/477405737-stability_work_in_progress.png)
![internaluse-green](https://bitbucket.org/repo/ekyaeEE/images/3847436881-internal_use_stable.png)
![issues-open](https://bitbucket.org/repo/ekyaeEE/images/2944199103-issues_open.png)

                                                 (...) a single JSON file can be used as a database.

# RATIONALE #

* A custom app to be available in the Windows, MacOSX  and Linux realm with a focus on simplicity and minimalism
* Must comply with 2 premises: closed architecture across operating systems (_search_ and _show_ the results) and strict offline mode, so any query done do not need to connect to any remote server
* This repo is a living document that will grow and adapt over time
* The principles of this project were driven by [The Zen of Python](https://en.wikipedia.org/wiki/Zen_of_Python)
![stand-alone.gif](https://lh4.googleusercontent.com/BNzgl3yg31sJsItuJJ0FqNOr7yteXttYWKo33KMf_X0CKtLGA_bTdbho4Pl9EYuW1-E=w2400)

> Searching this terms: `Africa`, `patagonia`

### What is this repository for? ###

* Quick summary
    - An application for search through up our library
* Version 1.01

### How do I get set up? ###

* Summary of set up
    - Check [colophon.md](https://bitbucket.org/imhicihu/bibliographical-searcher-stand-alone-app/src/master/Colophon.md)
* Configuration
    - ["Don't expect that you can build app for all platforms on one platform."](https://www.electron.build/multi-platform-build)
* Packages handlers _et alia_
    - [Node.js](https://nodejs.org/)
    - [NPM](https://www.npmjs.com/)
        + Packages:
              - [nvm](https://github.com/creationix/nvm)
              - Lint
              - Debug
              - [Devtron](https://github.com/electron/devtron)
              - [electron-db](https://github.com/alexiusacademia/electron-db)
    - [Homebrew](https://brew.sh/)
    - [Yarn](https://yarnpkg.com/)
    - [Gulp](https://gulpjs.com) (javascript task automatization)
* Database
    - Check our digital [repository](https://bitbucket.org/imhicihu/databases-repositories)

### Related repositories ###

* Some repositories linked with this project:
     - [Migration data](https://bitbucket.org/imhicihu/migration-data-checklist/src/)
     - [Terrae database](https://bitbucket.org/imhicihu/terrae-database/src/)
     - [Bibliographical hybrid (mobile app)](https://bitbucket.org/imhicihu/bibliographical-hybrid-mobile-app/src/)
     - [Bibliographical database (migration)](https://bitbucket.org/imhicihu/bibliographical-database-migration/src/)
     - [Bibliographic data on iOS devices](https://bitbucket.org/imhicihu/bibliographic-data-on-ios-devices/src/)
     - [NPM under proxy (settings)](https://bitbucket.org/imhicihu/npm-under-proxy-settings/src/)
     - [Setting up Github under proxy](https://bitbucket.org/imhicihu/setting-up-github-under-proxy/src/)
     - [Migration data between different MacOS environments (Checklist)](https://bitbucket.org/imhicihu/migration-data-between-different-macos-environments-checklist/src/)  

### Source ###

* Check them on [here](https://bitbucket.org/imhicihu/bibliographical-searcher-stand-alone-app/src)

### Issues ###

* Check them on [here](https://bitbucket.org/imhicihu/bibliographical-searcher-stand-alone-app/issues)

### Changelog ###

* Please check the [Commits](https://bitbucket.org/imhicihu/bibliographical-searcher-stand-alone-app/commits/) section for the current status

### Who do I talk to? ###

* Repo owner or admin
    - Contact `imhicihu` at `gmail` dot `com`
* Other community or team contact
    - Contact is _enable_ on the [board](https://bitbucket.org/imhicihu/bibliographical-searcher-stand-alone-app/addon/trello/trello-board) of this repo. (You need a [Trello](https://trello.com/) account)

### Code of Conduct

* Please, check our [Code of Conduct](https://bitbucket.org/imhicihu/bibliographical-searcher-stand-alone-app/src/master/code_of_conduct.md)

### Legal ###

* All trademarks are the property of their respective owners.

### License ###

* The content of this project itself is licensed under the ![MIT Licence](https://bitbucket.org/repo/ekyaeEE/images/2049852260-MIT-license-green.png) 