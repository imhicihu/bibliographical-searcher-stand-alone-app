* https://github.com/electron-userland/
* https://css.gg/  --) minimalist icons, even there is an API to made calls
* https://github.com/dfahlander/Dexie.js#hello-world-es2015--es6
* https://www.techiediaries.com/electron-data-persistence/  => Leído  (take in count the tutorial about insert an sqlite3 database)
* https://blog.logrocket.com/building-an-offline-first-app-with-react-and-rxdb-e97a1fa64356  (leído, tener en cuenta data acerca de los _schemas_)
* [Understanding JavaScripts async await](https://ponyfoo.com/articles/understanding-javascript-async-await): some hints and advice about asynchronous connections
* ~~https://www.youtube.com/watch?v=c76FTxLRwAw~~  > out of scope, how to connect a sqlite3 database by `knex.js`
* https://www.laurivan.com/make-electron-work-with-sqlite3/ (read on 2019/03/19) follow the advice about the creation of the database (some issues can be acomplished ~~instead~~ _so_ it's the rebuilding time)
* https://github.com/ptmt/react-native-macos  (take in count the "component examples" at the bottom of the pages on `RNTesterApp`)
* https://github.com/pubkey/rxdb/tree/master/examples/electron  
* https://youmightnotneedelectron.com/
* https://github.com/electron-userland/electron-packager#building-windows-apps-from-non-windows-platforms/
* ttps://fusejs.io/  -> fuzzy searcher 