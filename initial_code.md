#### First stage
```
   # Clone this repository
   git clone https://bitbucket.org/imhicihu/bibliographical-searcher-stand-alone-app/electron-quick-start
   # Install Electron Packager (if first time)
   npm install electron-packager -g 
   # Go into the repository
   cd electron-quick-start
   # Install dependencies
   npm install
   # Run the app
   npm start
   # Build the Executable/App
   cd electron-quick-start
   npm run package-win
   OR
   npm run package-mac
```